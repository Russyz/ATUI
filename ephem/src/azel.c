#include <stdio.h>
#include <math.h>
#include "ephem.h"

/* Takes in hour angle and topocentric declination (both in degrees), */
/* and computes the azimuth and elevation of the coordinates at the   */
/* location of APO, as defined in apo.h.  Az and el are in degrees. */

void azel(double hourang, double topodelt, double *azimuth, double *elev)
{
  double numer,denom,arg;

  if (hourang < -180.0) hourang += 360.0;
  hourang *= DTR;
  topodelt *= DTR;
  numer = sin(hourang);
  denom = cos(hourang)*sin(APOLAT*DTR);
  denom -= tan(topodelt)*cos(APOLAT*DTR);
  *azimuth = 180.0 + RTD * atan2(numer,denom);
  *azimuth -= 360.0*floor(*azimuth/360.0);
  arg = sin(APOLAT*DTR)*sin(topodelt);
  arg += cos(APOLAT*DTR)*cos(topodelt)*cos(hourang);
  *elev = RTD * asin(arg);

}
