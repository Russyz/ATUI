#include <stdio.h>
#include <math.h>
#include "ephem.h"

int main()
{
  double st,alpha,delta,range,topodelt,topora,latitude,elevation;

  st = 45.0;
  alpha = 50.0;
  delta = 20.0;

  range = 1.1111111111111 * 6378.136;

  latitude = 30.0;
  elevation = 0.0;

  topo(&st,&alpha,&delta,&range,&latitude,&elevation,&topodelt,&topora);

  printf("new RA, dec. = %lf, %lf\n",topora,topodelt);
  printf("Delta-alpha = %lf\n",topora-alpha);

  return 0;
}
