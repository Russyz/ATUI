#include <stdio.h>
#include <math.h>
#include "ephem.h"

/* transforms from apparent ecliptic coords (longitude corrected for  */
/* nutation) to apparent equatorial (R.A. and dec.), using the true   */
/* obliquity of the ecliptic for the date.                            */
/*                                                                    */
/* input is JD in days, all angles in in radians, out in degrees      */

void ecliptoeq(double jd, double lambda, double beta, double *alpha, double *delta)
{
  double psi,eps,obliq;

  nutation(jd,&psi,&eps,&obliq);
  obliq += eps/3600.0;
  obliq *= DTR;

  if (cos(lambda) == 0.0) {
    *alpha = lambda;
  } else {
    *alpha = atan((sin(lambda)*cos(obliq)-tan(beta)*sin(obliq))/cos(lambda));
  }

  *delta = asin(sin(beta)*cos(obliq) + cos(beta)*sin(obliq)*sin(lambda));

  *alpha *= RTD;
  *delta *= RTD;

  if (cos(lambda) < 0.0) {
    *alpha += 180.0;
  }

  *alpha -= 360.0*floor(*alpha/360.0);

/*  printf("lambda = %lf, obliquity = %lf\n",lambda,obliq*RTD); */

}
