#include <stdio.h>
#include <math.h>
#include "ephem.h"

/* returns mean siderial time for the given julian date.       */
/* does not correct for nutation to give apparent siderial     */
/* time.  This is accomplished by adding Dpsi * cos(obliq)     */

double siderial(double jd)
{
  double djd,fp,bt,st;
  long int ip;

  djd = jd - 2451545.0;
  ip = floor(djd);
  fp = djd - ip;
  bt = djd/36525.0;

  st = 280.46061837 + 360.98564736629*fp + 0.98564736629*ip;
  st += (0.000387933 - (bt/38710000.0))*bt*bt;

  st -= 360.0*floor(st/360.0);

  return st;

}
