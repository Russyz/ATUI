#include "ephem.h"

void mm3x3(double a[], double b[], double c[])

/* Takes two 3x3 matrix pointers, a, b, stored in 1-d arrays nine	*/
/* elements long (row major, such that elements 0,1,2 go across a	*/
/* row, and 0,3,6 go down a column), and multiplies a*b = c.  Note	*/
/* that order matters: a*b is not the same as b*a.			*/

{

  double *cptr;
  int i,j;

  cptr = c;

  for (i=0; i<3; i++){
    for (j=0; j<3; j++){
      *cptr++ = a[3*i]*b[j] + a[3*i+1]*b[j+3] + a[3*i+2]*b[j+6];
    }
  }

  /*  *cptr++ = a[0]*b[0] + a[1]*b[3] + a[2]*b[6];	*/
  /*  *cptr++ = a[0]*b[1] + a[1]*b[4] + a[2]*b[7];	*/
  /*  *cptr++ = a[0]*b[2] + a[1]*b[5] + a[2]*b[8];	*/
  /*							*/
  /*  *cptr++ = a[3]*b[0] + a[4]*b[3] + a[5]*b[6];	*/
  /*  *cptr++ = a[3]*b[1] + a[4]*b[4] + a[5]*b[7];	*/
  /*  *cptr++ = a[3]*b[2] + a[4]*b[5] + a[5]*b[8];	*/
  /*							*/
  /*  *cptr++ = a[6]*b[0] + a[7]*b[3] + a[8]*b[6];	*/
  /*  *cptr++ = a[6]*b[1] + a[7]*b[4] + a[8]*b[7];	*/
  /*  *cptr++ = a[6]*b[2] + a[7]*b[5] + a[8]*b[8];	*/
  
}
