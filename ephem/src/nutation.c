#include <stdio.h>
#include <math.h>
#include <string.h>
#include "ephem.h"

void nutation(double jd, double *psi, double *eps, double *obliq)
{
  double dt,jde,bt,D,M,MP,F,OM,arg,multpsi,multeps;
  double amppsi1,ampeps1,sumpsi,sumeps;
  long int amppsi0,ampeps0;
  int m1,m2,m3,m4,m5;
  char str[80], *path=DATADIR;
  FILE *fp;

  dt = dyntime(jd);
  jde = jd + dt/864000.0;
  bt = (jde - 2451545.0) / 36525.0;

/*  printf("DT = %lf; T = %15.12lf\n",dt,bt); */

  D = 297.85036 + (445267.111480 - (0.0019142 - (bt/189474.0))*bt)*bt;

  M = 357.52772 + (35999.050340 - (0.0001603 + (bt/300000.0))*bt)*bt;

  MP =134.96298 + (477198.867398 + (0.0086972 + (bt/56250.0))*bt)*bt;

  F = 93.27191 + (483202.017538 - (0.0036825 - (bt/327270.0))*bt)*bt;

  OM = 125.04452 - (1934.136261 - (0.0020708 + (bt/450000.0))*bt)*bt;

  D  -= 360.0*floor(D/360.0);
  M  -= 360.0*floor(M/360.0);
  MP -= 360.0*floor(MP/360.0);
  F  -= 360.0*floor(F/360.0);
  OM -= 360.0*floor(OM/360.0);

/*  printf("D  = %lf\n",D); */
/*  printf("M  = %lf\n",M); */
/*  printf("MP = %lf\n",MP); */
/*  printf("F  = %lf\n",F); */
/*  printf("OM = %lf\n",OM); */

  sumpsi = 0.0;
  sumeps = 0.0;

  
  fp = myopen(path,"nut.dat","r");
  fgets(str,80,fp);
  while (strstr(str,"END") == NULL) {
    sscanf(str,"%d\t%d\t%d\t%d\t%d\t%ld\t%lf\t%ld\t%lf",
           &m1,&m2,&m3,&m4,&m5,&amppsi0,&amppsi1,&ampeps0,&ampeps1);
    multpsi = ((double) amppsi0 + amppsi1*bt) * 0.0001;
    multeps = ((double) ampeps0 + ampeps1*bt) * 0.0001;
    arg = (m1*D + m2*M + m3*MP + m4*F + m5*OM) * DTR;
    sumpsi += multpsi * sin(arg);
    sumeps += multeps * cos(arg);
    fgets(str,80,fp);
  }
  fclose(fp);

  *eps = sumeps;
  *psi = sumpsi;

/*  printf("Nutation in longitude, latitude is: %lf, %lf\n",sumpsi,sumeps); */

  *obliq = 23.4392911111 - (46.8150 + (0.00059 - (0.001813*bt))*bt)*bt/3600.0;

/*  printf("eps = %lf; true ecliptic = %lf\n",eps0,eps); */

}
