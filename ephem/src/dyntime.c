#include <stdio.h>
#include <math.h>
#include "ephem.h"

double dyntime(double julianday)
{
  double jd,fp,day,arg,t,dt,jde;
  int c,e,month,year;
  long int ip,a,alpha,b,d;

  jd = julianday + 0.5;
  ip = floor(jd);
  fp = jd - ip;

  if (ip < 2299161) {
    a = ip;
  } else {
    alpha = floor(((double) ip - 1867216.25)/36524.25);
    a = ip + 1 + alpha - floor((double) alpha / 4.0);
  }

  b = a + 1524;
  c = floor(((double) b - 122.1)/365.25);
  d = floor(365.25 * c);
  e = floor((double) (b - d) / 30.6001);

  day = b - d - floor(30.6001 * e) + fp;

  if (e < 14) {
   month = e - 1;
  } else {
    month = e - 13;
  }

  if (month > 2) {
    year = c - 4716;
  } else {
    year = c - 4715;
  }

  t = (double) (year - 2000) / 100.0;

  if (year < 948) {
    dt = 2177 + 497*t + 44.1*t*t;
  } else {
    dt = 102 + 102*t + 25.3*t*t;
  }
  if (year > 1997 && year < 2100){
    dt += 0.37*((double) (year - 2100));
  }

  arg = (jd - 2415020.5)/36525.0;
  if (year > 1899 && year < 1998) {
    dt = -2.44 + arg*(87.24 + arg*(815.20 - arg*(2637.80 + arg*(18756.33
         - arg*(124906.66 - arg*(303191.19 - arg*(372919.88 - arg*(232424.66
         - 58353.42*arg))))))));
  }
  if (year > 1799 && year < 1900) {
    dt = -2.50 + arg*(228.95 + arg*(5218.61 + arg*(56282.84 + arg*(324011.78
         + arg*(1061660.75 + arg*(2087298.89 + arg*(2513807.78
         + arg*(1818961.41 + arg*(727058.63 + arg*123563.95)))))))));
  }
  jde = jd + dt/86400.0;

  return dt;

}

