#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "ephem.h"

int main(int argc, char *argv[])
{
  double second,epoch,incl,omega,peri,meananom,meanmot,ecc,decel,jerk;
  double m,mdot,em,nu,nudot,ct,st,a,b,c,d,ra,dec,radot,decdot;
  double h,alpha,delta,period,tmp,ss,ss2;
  double yday,day,jdc,jd1,dt,dut1,jde,mjd,mjdtai,zeroth=0.0;
  double psi,eps,obliq,range,topodelt,topora,hourang,azimuth,elev;
  double stationlat,stationelev,temp,press,ref;
  int month,intday,year,jan=1,epochyear,hour,minute,hh,hh2,mm,mm2;
  char garb1[20],garb2[20],jerkstr[6],eccstr[7];
  char *path=DATADIR, file[20], filename[80];
  int i,orbs,yy,jerkexp;
  long int jerkint,eccint,mjdin;

  FILE *fp;
  char str[80],sat[15];
  char sp[] = " ",lclag[]="lageos",uclag[]="LAGEOS";
  char s1[20],s2[20],s3[20],s4[20],s5[20],s6[20],s7[20],s8[20],s9[20];

  struct tm *tp;
  time_t timenow;

  #ifdef __MACOS__
    #include <console.h>
    argc = ccommand(&argv);
  #endif

  if (argc < 2) {
    printf("usage:  sat SATELLITE month day year hour minute second\n");
    strcpy(sat,"LAGEOS 1");
  }

  if (argc > 1) {
    sscanf(argv[1],"%s",sat);
/*    if (strstr(sat,uclag) == NULL && strstr(sat,lclag) == NULL) { */
/*      printf("Must specify LAGEOS1 or LAGEOS2\n"); */
/*      return -1; */
/*    } */
    if (islower(sat[0]) != 0) {
      i=0;
      while (sat[i] != '\0') {
        sat[i] = toupper(sat[i]);
        i++;
      }
    }
    if (strstr(sat,uclag) != NULL){
      strcat(uclag,sp);
      strcat(uclag,&sat[6]);
      strcpy(sat,uclag);
    }
  }

  if (argc < 8) {
    timenow = time(NULL);
    tp = gmtime(&timenow);
    month = (*tp).tm_mon+1;
    intday = (*tp).tm_mday;
    year = (*tp).tm_year + 1900;
    hour = (*tp).tm_hour;
    minute = (*tp).tm_min;
    second = (*tp).tm_sec;
  } else {
    sscanf(argv[2],"%d",&month);
    sscanf(argv[3],"%d",&intday);
    sscanf(argv[4],"%d",&year);
    sscanf(argv[5],"%d",&hour);
    sscanf(argv[6],"%d",&minute);
    sscanf(argv[7],"%lf",&second);
  }

  day = intday + (hour + (minute + second/60.0)/60.0)/24.0;
  jdc = julianday(month,day,year);
  mjd = jdc - 2400000.5;

  dt = dyntime(jdc);
  if (mjd > 52058.5 && mjd < 52423.5) {
    fp = myopen(path,"dut1.dat","r");
    fgets(str,80,fp);
    while (strstr(str,"END") == NULL) {
      sscanf(str,"%s%s%s%s%s%s%s",s1,s2,s3,s4,s5,s6,s7);
      sscanf(s4,"%ld",&mjdin);
      sscanf(s7,"%lf",&tmp);
      if (fabs(mjd - (double) mjdin) < 0.5) {
        dut1 = tmp;
      }
      fgets(str,80,fp);
    }
    fclose(fp);
    dt = TDT_TAI + TAI_UTC - dut1;
  }
  dut1 = TDT_TAI + TAI_UTC - dt;

  jde = jdc + dt/86400.0;
  mjdtai = (jdc + TAI_UTC/86400.0 - 2400000.5)*86400.0;
  jd1 = jdc - dut1/86400.0;

  printf("Calculating for %s on %02u/%02u/%4u at %02u:%02u:%07.4lf UTC\n",
          sat,month,intday,year,hour,minute,second);
  printf("JD = %.6lf; JDE= %.6lf; MJDTAI = %lf\n",jdc,jde,mjdtai);

  fp = myopen(path,"tle.dat","r");
  fgets(str,80,fp);
  while (strstr(str,sat) == NULL){
    fgets(str,80,fp);
/*    if (strstr(str,EOF) != NULL){
      printf("Failed to find %s in two-line element file\n",sat);
      return -1;
    } */
  }
  fgets(str,80,fp);
  sscanf(str,"%s%s%s%s%s%s%s%s%s",s1,s2,s3,s4,s5,s6,s7,s8,s9);
  sscanf(s4,"%2d%lf",&yy,&yday);
  sscanf(s5,"%lf",&decel);
  sscanf(s6,"%5ld%2d",&jerkint,&jerkexp);
  jerk = (double) jerkint / 100000.0 * pow(10.0,jerkexp);

  fgets(str,80,fp);
  sscanf(str,"%s%s%s%s%s%s%s%s",s1,s2,s3,s4,s5,s6,s7,s8);
  sscanf(s3,"%lf",&incl);
  sscanf(s4,"%lf",&omega);
  sscanf(s5,"%ld",&eccint);
  sscanf(s6,"%lf",&peri);
  sscanf(s7,"%lf",&meananom);
  sscanf(s8,"%lf%6c",&meanmot);
  ecc = (double) eccint / 10000000.0;

  fclose(fp);

/*  printf("%d %f %g %g\n",yy,yday,decel,jerk); */
/*  printf("%f %f %g %f %f %lf\n",incl,omega,ecc,peri,meananom,meanmot); */

  if (yy < 65){
    epochyear = yy + 2000;
  } else {
    epochyear = yy + 1900;
  }
  epoch = julianday(jan,zeroth,epochyear);
  epoch += yday;

  printf("Epoch is %lf; offset is %lf days\n",epoch,jdc-epoch);

  orbs = floor((meanmot + decel*(jdc-epoch))*(jdc-epoch));
  m = meananom + 360.0*((meanmot + decel*(jdc-epoch))*(jdc-epoch) - orbs);
  m -= 360.0 * floor(m/360.0);
  mdot = 360.0*(meanmot-2.0*decel*(jdc-epoch))/86400.0;

  printf("mean anomaly = %lf; mdot = %lf\n",m,mdot);

  m *= DTR;
  mdot *= DTR;

/* now account for satellite's eccentricity, translating mean
   anomaly (m) into true anomaly (nu) via eccentric anomaly (em) */

  em = m;
  delta = -ecc*sin(em);
  period = 86400.0 / meanmot;
  a = pow(GMEARTH*period*period/(4*PI*PI),0.333333333333);
  h = a - REARTH;

  i = 0;
  while (fabs(delta) > (h/a)/RTARC) {
    delta = em - ecc*sin(em) - m;
    printf("Iteration %2d has offset of %lf arcseconds\n",i++,delta*RTARC);
    em -= delta / (1.0 - ecc*cos(em));
  }

  em -= 2.0 * PI * floor(em/(2.0 * PI));
  nu = 2.0*atan(sqrt((1.0+ecc)/(1.0-ecc))*tan(em/2.0));
  if (em > PI) { nu += 2.0 * PI; }

  printf("Anomalies: E = %lf; nu = %lf\n",em*RTD, nu*RTD);
  
  nudot = mdot * sqrt(1.0-ecc*ecc)/pow(1.0-ecc*cos(em),2);

  range = a*(1.0 - ecc*ecc)/(1000.0*(1 + ecc*cos(nu)));

  incl *= DTR;
  omega *= DTR;
  peri *= DTR;
  ct = cos(nu);
  st = sin(nu);

  dec = asin(ct*sin(incl)*sin(peri) + st*sin(incl)*cos(peri));

  a = sin(omega)*cos(peri) + cos(omega)*cos(incl)*sin(peri);
  b = cos(omega)*cos(incl)*cos(peri) - sin(omega)*sin(peri);
  c = cos(omega)*cos(peri) - sin(omega)*cos(incl)*sin(peri);
  d = cos(omega)*sin(peri) + sin(omega)*cos(incl)*cos(peri);

  ra = atan2(a*ct + b*st, c*ct - d*st);
  ra -= 2.0*PI * floor(ra /(2.0 *PI));

  decdot = (ct*cos(peri) - st*sin(peri))*sin(incl);
  decdot /= sqrt(1.0-pow(sin(incl)*(ct*sin(peri) + st*cos(peri)),2));
  decdot *= nudot;

  radot = (b*ct-a*st)*(c*ct-d*st) + (a*ct+b*st)*(c*st+d*ct);
  radot /= (c*ct-d*st)*(c*ct-d*st) + (a*ct+b*st)*(a*ct+b*st);
  radot *= nudot;

  printf("\ndec = %f; decdot = %11.8lf\n",dec*RTD,decdot*RTD);
  printf(" RA = %f;  RAdot = %11.8lf\n",ra*RTD,radot*RTD);

  alpha = ra * RTD;
  delta = dec * RTD;

  nutation(jde,&psi,&eps,&obliq);

  st = siderial(jd1) + APOLONG;
  st += psi*cos((obliq + eps/3600.0)*DTR)/3600.0;
  st -= 360.0*floor(st/360.0);
  tmp = st/15.0;
  hms(tmp,&hh,&mm,&ss);
  printf("\nApparent Siderial time is: %12.8lf = %02u:%02u:%07.4lf\n",
           st,hh,mm,ss);

  tmp = alpha / 15.0;
  hms(tmp,&hh,&mm,&ss);
  hms(delta,&hh2,&mm2,&ss2);
  printf("\nApparent RA,dec: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",
          alpha,delta,hh,mm,ss,hh2,mm2,ss2);
/*  printf("\nApparent geocentric RA (dec. hours), dec (deg.): (%lf, %lf)\n",*/
/*            alpha/15.0,delta);*/

  printf("Geocentric range = %lf = %10.8lf A.U.\n",range,range/149597870.66);

  stationlat = APOLAT;
  stationelev = APOELEV;
  topo(st,alpha,delta,range,stationlat,stationelev,&topodelt,&topora);

  tmp = topora / 15.0;
  hms(tmp,&hh,&mm,&ss);
  hms(topodelt,&hh2,&mm2,&ss2);
/*  printf("\nTopocentric RA,dec: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",	*/
/*          topora,topodelt,hh,mm,ss,hh2,mm2,ss2);	*/
  printf("\nTopocentric RA, dec: (%lf, %lf) => (%lf, %lf)\n",
            topora,topodelt,topora/15.0,topodelt);

  hourang = st - topora;
  azel(hourang,topodelt,&azimuth,&elev);

  temp = 283.0;
  press = 717.0;
  ref = refraction(elev,temp,press);
  tmp = elev + ref;
/*  azimuth += 180.0;*/
  if (azimuth > 360.0) azimuth -= 360.0;
  hms(azimuth,&hh,&mm,&ss);
  hms(tmp,&hh2,&mm2,&ss2);
  printf("Azimuth, elevation: (%lf, %lf); (%02u:%02u:%05.2lf, %d:%02u:%04.1lf)\n",
          azimuth,elev+ref,hh,mm,ss,hh2,mm2,ss2);

  if (fabs(ref) > 0.0) {
    printf("Elevation refracted by %lf arcsec\n",ref*3600.0);
  }

  return 0;

}

