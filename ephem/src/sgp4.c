#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <ctype.h>
#include "ephem.h"
#include "sgp4.h"

int sgp4(double incl, double ranode, double ecc, double peri,
          double meananom, double meanmot, double bstar, double tsince,
          double x3[], double v3[])
{
  double CK2,CK4,Q0MS2T,Q0MS24,S;
  double n0,e0,i0,w0,node0,m0,e02,beta0,beta02;
  double cosi0,sini0,theta2,theta4,x3thm1,x1mth2,x1m5th,x7thm1;
  double a1,delta1,a0,delta0,npp,app,a3;
  double s4,perigee,pinvsq,tsi,eta,etasq,eeta,psisq;
  double coef,coef1,c1,c2,c3,c4,c5,d2,d3,d4,mdf,omgadf,noddf;
  double temp,temp1,temp2,temp3,temp4,temp5,temp6,tempa,tempe,templ;
  double mdot,omgdot,nodot,omgcof,mcof,nodcof,lcof,aycof,delm0,sinm0;
  double tsq,tcube,tfour,t2cof,t3cof,t4cof,t5cof,c1sq;
  double omega,mp,node,delomg,delm,a,e,l,pl,r,beta,n,axn,ayn,aynl,ll,lt;
  double capu,delta,sinepw,cosepw,epw,ecose,esine,elsq,rdot,rfdot;
  double betal,cosu,sinu,u,cos2u,sin2u,rk,uk,nodek,inck,rdotk,rfdotk;
  double sinuk,cosuk,sinik,cosik,sinnok,cosnok,mx,my,ux,uy,uz,vx,vy,vz;
  double x,y,z,xdot,ydot,zdot,*x3ptr,*v3ptr;

  int isimp,iflag;

/* Set some constants							*/

  CK2 = XJ2 * AE * AE / 2.0;
  CK4 = -3.0 * XJ4 * pow(AE,4) / 8.0;
  Q0MS2T = pow((Q0 - S0)*AE/XKMPER,4.0);
  S = AE * (1.0 + S0/XKMPER);

/* Fake an element set							*/


/* Convert Two-line element format into radians, rads/min, etc.		*/

  n0 = meanmot * TWOPI / XMNPDA;
  e0 = ecc;
  i0 = incl * DTR;
  w0 = peri * DTR;
  node0 = ranode * DTR;
  m0 = meananom * DTR;

  cosi0 = cos(i0);
  sini0 = sin(i0);
  theta2 = cosi0 * cosi0;
  theta4 = theta2 * theta2;
  x3thm1 = 3.0*theta2 - 1.0;
  x1mth2 = 1.0 - theta2;
  x1m5th = 1.0 - 5.0*theta2;
  x7thm1 = 7.0*theta2 - 1.0;
  e02 = e0 * e0;
  beta02 = 1.0 - e02;
  beta0 = sqrt(beta02);

  a1 = pow(XKE/n0, 2.0/3.0);
  delta1 = 1.5 * CK2 * x3thm1;
  delta1 /= a1*a1*beta0*beta02;
  a0 = a1 * (1.0 - (1.0/3.0 + (1.0 + (134.0*delta1/81.0))*delta1)*delta1);
  delta0 = 1.5 * CK2 * x3thm1;
  delta0 /= a0*a0*beta0*beta02;
  npp = n0 / (1.0 + delta0);
  app = a0 / (1.0 - delta0);

  isimp = 0;
  if ((app*(1.0-e0)/AE) < (220.0/XKMPER + AE)) isimp = 1;

  s4 = S;
  Q0MS24 = Q0MS2T;
  perigee = (app*(1.0 - e0) - AE) * XKMPER;
  if (perigee < 156.0) s4 = perigee - S0;
  if (perigee < 98.0) s4 = 20.0;
  if (perigee < 156.0) {
    s4 = s4/XKMPER + AE;
    Q0MS24 = pow((Q0 - s4)*AE/XKMPER,4.0);
  }
  
  pinvsq = 1.0 / (app*app*beta02*beta02);
  tsi = 1.0 / (app - s4);
  eta = app*e0*tsi;
  etasq = eta*eta;
  eeta = e0*eta;
  psisq = fabs(1.0 - etasq);
  coef = Q0MS24 * pow(tsi,4);
  coef1 = coef / pow(psisq,3.5);
  c2 = app*(1.0+1.5*etasq+eeta*(4.0+etasq));
  c2 += 0.75*CK2*tsi*x3thm1*(8.0 + 3.0*etasq*(8.0 + etasq))/psisq;
  c2 *= coef1*npp;
  c1 = c2 * bstar;
  a3 = -(XJ3/CK2)*AE*AE*AE;
  c3 = coef*tsi*a3*npp*AE*sini0/e0;
  c4 = -3.0*x3thm1*(1.0-2.0*eeta+etasq*(1.5-0.5*eeta));
  c4 += 0.75*x1mth2*(2.0*etasq-eeta*(1.0+etasq))*cos(2.0*w0);
  c4 *= -2.0*CK2*tsi / (app*psisq);
  c4 += eta*(2.0+0.5*etasq)+e0*(0.5+2.0*etasq);
  c4 *= 2.0*coef1*app*npp*beta02;
  c5 = 2.0*coef1*app*beta02*(1.0+2.75*(etasq+eeta)+eeta*etasq);
  
  temp1 = 3.0*CK2*pinvsq*npp;
  temp2 = temp1*CK2*pinvsq;
  temp3 = 1.25*CK4*pinvsq*pinvsq*npp;

  mdot = npp + 0.5*temp1*beta0*x3thm1;
  mdot += 0.0625*temp2*beta0*(13.0 - 78.0*theta2 + 137.0*theta4);

  omgdot = -0.5*temp1*x1m5th + 0.0625*temp2*(7.0-114.0*theta2+395.0*theta4);
  omgdot += temp3*(3.0 - 36.0*theta2 + 49.0*theta4);

  nodot = 0.5*temp2*(4.0-19.0*theta2) - temp1 + 2.0*temp3*(3.0-7.0*theta2);
  nodot *= cosi0;

  omgcof = bstar*c3*cos(w0);
  mcof = -2.0*coef*bstar*AE/(3.0*eeta);
  nodcof = -3.5*beta02*c1*temp1*cosi0;
  t2cof = 1.5*c1;
  lcof = 0.125*a3*sini0*(3.0+5.0*cosi0)/(1.0+cosi0);
  aycof = 0.25*a3*sini0;
  delm0 = pow(1.0 + eta*cos(m0),3);
  sinm0 = sin(m0);
  
  if (isimp == 0) {
    c1sq = c1*c1;
    d2 = 4.0*app*tsi*c1sq;
    temp = d2*tsi*c1/3.0;
    d3 = (17.0*app + s4)*temp;
    d4 = 0.5*temp*app*tsi*(221.0*app + 31.0*s4)*c1; 
    t3cof = d2 + 2.0*c1sq;
    t4cof = 0.25*(3.0*d3 + c1*(12.0*d2 + 10.0*c1sq));
    t5cof = 0.2*(3.0*d4 + 12.0*c1*d3 + 6.0*d2*d2 + 15.0*c1sq*(2.0*d2 + c1sq));
  }
  iflag = 0;

/* Update for Secular gravity and atmospheric drag		*/

  mdf = m0 + mdot * tsince;
  omgadf = w0 + omgdot * tsince;
  noddf = node0 + nodot * tsince;
  omega = omgadf;
  mp = mdf;
  tsq = tsince * tsince;
  tcube = tsince * tsq;
  tfour = tsq * tsq;
  node = noddf + nodcof*tsq;
  tempa = 1.0 - c1*tsince;
  tempe = bstar*c4*tsince;
  templ = t2cof*tsq;
  if (isimp == 0) {
    delomg = omgcof*tsince;
    delm = mcof*(pow(1.0 + eta*cos(mdf),3) - delm0);
    temp = delomg + delm;
    mp = mdf + temp;
    omega = omgadf - temp;
    tempa -= d2*tsq + d3*tcube + d4*tfour;
    tempe += bstar*c5*(sin(mp)-sinm0);
    templ += t3cof*tcube + tfour*(t4cof + tsince*t5cof);
  }

  a = app*tempa*tempa;
  e = e0-tempe;
  l = mp + omega + node + npp*templ;
  beta = sqrt(1.0 - e*e);
  n = XKE / pow(a,1.5);

/* Long Period Periodics					*/

  axn = e*cos(omega);
  temp = 1.0/(a*beta*beta);
  ll = temp*lcof*axn;
  aynl = temp*aycof;
  lt = l + ll;
  ayn = e*sin(omega) + aynl;

/* Solve Kepler's Equation to E6A accuracy			*/

  capu = lt - node;
  capu -= TWOPI * floor(capu / TWOPI);
  temp2 = capu;
  delta = 1.0;
  while ( delta > E6A ) {
    sinepw = sin(temp2);
    cosepw = cos(temp2);
    temp3 = axn*sinepw;
    temp4 = ayn*cosepw;
    temp5 = axn*cosepw;
    temp6 = ayn*sinepw;
    epw = (capu - temp4 + temp3 - temp2)/(1.0 - temp5 - temp6) + temp2;
    delta = fabs(epw - temp2);
    temp2 = epw;
  }

/* Short Period Preliminary Quantities				*/

  ecose = temp5 + temp6;
  esine = temp3 - temp4;
  elsq = axn*axn + ayn*ayn;
  temp = 1.0 - elsq;
  pl = a*temp;
  r = a*(1.0 - ecose);
  temp1 = 1.0 / r;
  rdot = XKE * sqrt(a) * esine *temp1;
  rfdot = XKE * sqrt(pl) * temp1;
  temp2 = a*temp1;
  betal = sqrt(temp);
  temp3 = 1.0 / (1.0 + betal);
  cosu = temp2*(cos(epw) - axn + ayn*esine*temp3);
  sinu = temp2*(sin(epw) - ayn - axn*esine*temp3);
  u = atan2(sinu,cosu);
  sin2u = 2.0*sinu*cosu;
  cos2u = 2.0*cosu*cosu - 1.0;
  temp = 1.0 / pl;
  temp1 = CK2*temp;
  temp2 = temp1*temp;

/* Update for short periodics					*/

  rk = r*(1.0 - 1.5*temp2*betal*x3thm1) + 0.5*temp1*x1mth2*cos2u;
  uk = u - 0.25*temp2*x7thm1*sin2u;
  nodek = node + 1.5*temp2*cosi0*sin2u;
  inck = i0 + 1.5*temp2*cosi0*sini0*cos2u;
  rdotk = rdot - n*temp1*x1mth2*sin2u;
  rfdotk = rfdot + n*temp1*(x1mth2*cos2u + 1.5*x3thm1);

/* orientation vetors						*/

  sinuk = sin(uk);
  cosuk = cos(uk);
  sinik = sin(inck);
  cosik = cos(inck);
  sinnok = sin(nodek);
  cosnok = cos(nodek);

  mx = -sinnok*cosik;
  my = cosnok*cosik;

  ux = mx*sinuk + cosnok*cosuk;
  uy = my*sinuk + sinnok*cosuk;
  uz = sinik*sinuk;

  vx = mx*cosuk - cosnok*sinuk;
  vy = my*cosuk - sinnok*sinuk;
  vz = sinik*cosuk;

/* Position and Velocity					*/

  x = rk*ux;
  y = rk*uy;
  z = rk*uz;

  xdot = rdotk*ux + rfdotk*vx;
  ydot = rdotk*uy + rfdotk*vy;
  zdot = rdotk*uz + rfdotk*vz;

  x3ptr = x3;
  v3ptr = v3;
  *x3ptr++ = x * XKMPER / AE;
  *x3ptr++ = y * XKMPER / AE;
  *x3ptr++ = z * XKMPER / AE;
  *v3ptr++ = xdot * (XKMPER / AE) * (XMNPDA / 86400.0);
  *v3ptr++ = ydot * (XKMPER / AE) * (XMNPDA / 86400.0);
  *v3ptr++ = zdot * (XKMPER / AE) * (XMNPDA / 86400.0);

/*  printf("%lf\t%14.8lf\t%14.8lf\t%14.8lf\n\t\t%14.8lf\t%14.8lf\t%14.8lf\n",*/
/*         tsince,x3[0],x3[1],x3[2],v3[0],v3[1],v3[2]);*/

  return iflag;
}
