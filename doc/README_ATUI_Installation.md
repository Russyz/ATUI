=== icc stream ===
$ telnet houston 5320
…
To exit, type “disconnect"

[//]: # (this is a comment)

Installing ATUI on Mac
======================

We are going to use brew to install packages. If you don't already have brew, use the following command to install -

```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Now, firstly check the version of Python on your Mac. Here is the thing, maybe we can make things work with Python 3.7 (or higher in the future), we only really NEED Python 2.7 at this point. So, we are going to setup the whole system with python 2.7. If you have python3 and want to maintain separate environments or run TUI with python 3.7 support, this might not work and I haven't worked that problem out so I really don't know how could that be done.
We can check Python version with `python --version` and the current location of python with `which python`.
So, making sure that python is 2.7, we now proceed to install root. Use -

```
brew install root --without-python --with-python@2
```

This will install root. I know it looks like a crazy command but what it does is, builds root with support for python 2.7 (`--with-python@2`) and without support for python 3.7 (`--without-python`) which we don't need. The problem is ROOT can only be built with support for one version of python (As an error will show if you try and execute `brew install root --with-python@2` and thus trying to install support for both 2.7 and 3.7 since by default ROOT WILL have support for python 3.7). So, using this combination of tags we choose to build ROOT with support only for python 2.7.

Now if required, install all required python packages using -

```
pip install scipy
pip install numpy
pip install matplotlib
```
At this point, you should be able to -
1. Run an interactive session of python.
2. Run an interactive session of root.
3. Execute `import ROOT`, `import numpy, scipy, matplotlib` on the interactive python session.

Now you should be all set. If you don't have wget (and on a Mac, you probably don't), use -

```
brew install wget
```

Download and unzip TUI  
Note: download page for TUI is:  

  https://www.apo.nmsu.edu/35m_operations/TUI-images/

```
   $ wget http://www.apo.nmsu.edu/35m_operations/TUI-images/files/TUI_2.6.0_Source.zip
   $ unzip TUI_2.6.0_Source.zip
   $ mv TUI_2.6.0_Source/ ~/TUI
```

Now, you have the vanilla version of TUI. From the Apllo directory, try to run -

```
python ./runtui.py
```

to run the Vanilla version of TUI. If it runs, you're good. If it doesn't run, it is probably missing a python package. Install it with pip.

Now we need to get ATUI. Download and setup using -

```
$ mkdir ~/TUIAdditions
$ cd ~/TUIAdditions
$ git clone https://gitlab.com/lhuangru/ATUI.git Apollo
```

Now this is important (and may be different in Mac compared to Linux) - Move this directory to `/Users/<username>/Library/Application Support/`. This is where ATUI must be setup.
Now, inside the `TUIAdditions/Apollo/` folder, we need to modify the `pathStuff.py` file. Open it with an editor and change the python path to the result of `which python` and the other paths to the location of `TUIAddition` directory. In addition, if not defined, define the `moonPos()` function as follows -

```
def moonPos():  # point to moon_pos.py executable                              
 return "/Users/<username>/Library/Application\ Support/TUIAdditions/Apollo/ephem/moon_pos.py"
```

Finally, go back to the folder where TUI is setup and now run `runtui.py` and ATUI should come up. Connect using the Status window. Go to the `ROOT` tab and click the `Connect to ROOT` button. Now in a new terminal window, navigate to the `TUIAdditions/Apollo` directory and run -

```
python ./Apollo_Plots_ROOT.py twm2.json
```

The ROOT plots should now come up.

Installing ATUI on LINUX (debian/ubuntu variants)
========================

Install ROOT and pyROOT
------------
```
$ apt-get install root-system
$ apt-get install libroot-bindings-python5.34
```
if the latter does not work, then try:  
`$ apt-cache search pyROOT`  
and install what that returns. For example, on my machine, this returns -

`libroot-bindings-python5.34 - Python extension for ROOT - runtime libraries`

which is the library installed above.

Test that ROOT and pyROOT are ok  
```
$ python
>>> import ROOT
>>> ROOT.TCanvas()
```
should open up a graphics window titled "c1".  If it does, you should  
be all set to proceed.

Install python, numpy, scipy
----------------------------
```
$ apt-get install python
$ apt-get install python-numpy
$ apt-get install python-scipy
```

Install TUI
-----------
(I put the native TUI code in my home folder:  `/home/jbattat/TUI`)

Download and unzip TUI  
Note: download page for TUI is:  
  https://www.apo.nmsu.edu/35m_operations/TUI-images/

```
   $ wget http://www.apo.nmsu.edu/35m_operations/TUI-images/files/TUI_2.6.0_Source.zip
   $ unzip TUI_2.6.0_Source.zip
   $ mv TUI_2.6.0_Source/ ~/TUI
```

To launch TUI:

```
   $ ~/TUI/runtui.py
```

The way the APOLLO interface works is as an Add-On to this TUI, which is what we will set up next.
However, at this point, one should be able to run the vanilla version of TUI using the command above.
Can create a short script called `tui` to handle this for you:

It is possible you might get errors of the form -

`ImportError: No module named <Module_Name>`

Just `apt-get install <Module_Name>`. I had to install matplotlib and a few others.
UPDATE - I installed this on my new Ubuntu 18.04 machine and encountered a new issue. If you see something along the lines of `cannot import name cbook` then this is a python 2.7 legacy issue. To solve this you need to uninstall and reinstall matplotlib but a regressed version -
```
pip uninstall matplotlib
pip install matplotlib==2.0.2
```

This should solve the issue.


```
   #!/bin/bash
   python ~/TUI/runtui.py
```
Put it on your path, and then you can start TUI from anywhere.

Install Pmw, Pillow
----------------------
```$ apt-get install python-pmw
$ apt-get install python-pil.imagetk
```

I had to use 

`sudo apt-get install python-imaging-tk`

Which also worked fine for me.

Get the ATUI code (this repository)
-----------------------------------
Clone this repository into ~/TUIAdditions/Apollo  
```
$ mkdir ~/TUIAdditions
$ cd ~/TUIAdditions
$ git clone https://gitlab.com/lhuangru/ATUI.git Apollo
```

Modify pathStuff.py
-------------------
In `~/TUIAdditions/Apollo/` open `pathStuff.py` in a text editor and ensure that each path points to the relevant location on your system. For examples, you can also see `pathStuff_example.py` in the same directory  
A quick note on the paths here. The paths in the original file cloned from ATUI orginally are in the Windows formatting, which have to be changed into the location of the `~/TUIAdditions/Apollo` location.
For example I had to change 

`path = "C:\\Documents and Settings\\NPL\\Application Data\\TUIAdditions\\APOLLO\\" # yes end backslashes!`

to

`path = "/home/sanchit/TUIAdditions/Apollo/" # yes end backslashes!`

And similarly for all other paths.

On linux, my `site-packages` folder is actually called `/usr/lib/python2.7/dist-packages/`

To find out where your python modules folder is:
```
 $ python
 >>> import PIL
 >>> PIL.__file__
 '/usr/lib/python2.7/dist-packages/PIL/__init__.pyc'
```

The path before "/PIL/..." is the python modules directory.

Bringing Up ROOT Plots on APOLLO Interface
------------------------------------------

On the APOLLO window of the TUI, once logged in, navigate to the "ROOT" tab and click on "Connect to ROOT". Open a terminal and enter the following command from the `~/TUIAdditions/Apollo` directory-

`$ python Apollo_Plots_ROOT.py twm2.json`

A separate window should pop up with all the APOLLO Plots. When the session is over, first click the "Disconnect" button on the "ROOT" tab followed by "File --> Quit" from the Plot Window to make a clean Exit.
